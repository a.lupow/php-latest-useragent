<?php

require './vendor/autoload.php';

$ua = new \UserAgent\Source\WhatIsMyBrowser(
  new \UserAgent\Cache\FileCache('./cache')
);

for ($i = 0; $i < 1000; $i++) {
  print_r($ua->getRandom() . "\r\n");
}
