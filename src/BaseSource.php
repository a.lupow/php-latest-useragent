<?php

namespace UserAgent;

use UserAgent\Cache\RuntimeCache;

abstract class BaseSource implements UserAgentSource {

  /**
   * User agents cache source.
   *
   * @var \UserAgent\CacheInterface
   */
  protected $cache;

  /**
   * BaseSource constructor.
   *
   * @param \UserAgent\CacheInterface $cache_backend
   *   A cache backend to be utilised.
   */
  public function __construct(CacheInterface $cache_backend = NULL) {
    if (!$cache_backend) {
      $cache_backend = new RuntimeCache();
    }

    $this->cache = $cache_backend;
  }

  /**
   * {@inheritDoc}
   */
  public function getBrowserRandom($target = self::TARGET_BROWSER_CHROME) {
    $all = $this->getBrowserAll($target);

    // Get random version first.
    $versions = array_keys($all);
    $version_index = mt_rand(0, count($versions) - 1);
    $version = $versions[$version_index];

    $agents = $all[$version];
    $agent_index = mt_rand(0, count($agents) - 1);

    return $agents[$agent_index];
  }

  /**
   * {@inheritDoc}
   */
  public function getBrowserLatest($target = self::TARGET_BROWSER_CHROME) {
    $all = $this->getBrowserAll($target);
    $agents = array_pop($all);

    return $agents[0];
  }

  /**
   * {@inheritDoc}
   */
  public function getRandom() {
    $browsers = [self::TARGET_BROWSER_FIREFOX, self::TARGET_BROWSER_CHROME, self::TARGET_BROWSER_EDGE];
    $index = mt_rand(0, count($browsers) - 1);

    return $this->getBrowserRandom($browsers[$index]);
  }

}
