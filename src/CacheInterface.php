<?php

namespace UserAgent;

interface CacheInterface {

  /**
   * Returns cache storage value
   *
   * @param array $path
   *   Path for a given variable.
   * @param mixed $default_value
   *   Default value to be returned.
   *
   * @return mixed
   *   Cache storage value or default value if none found.
   */
  public function getCache(array $path = NULL, $default_value = NULL);

  /**
   * Sets cache value for a given path.
   *
   * @param $cache
   *   The cached value.
   * @param array $path
   *   Optional cache storage path.
   *
   * @return $this
   */
  public function setCache($cache, array $path = NULL);

}
