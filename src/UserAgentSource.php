<?php

namespace UserAgent;

interface UserAgentSource {

  const TARGET_BROWSER_CHROME = 'chrome';
  const TARGET_BROWSER_FIREFOX = 'firefox';
  const TARGET_BROWSER_EDGE = 'edge';

  /**
   * Fetches a list of available user agents for the target browser.
   *
   * @param string $target
   *   The target browser.
   *
   * @return string[][]
   *   A set of user agent strings.
   */
  public function getBrowserAll($target = self::TARGET_BROWSER_CHROME);

  /**
   * Fetches a random user agent for the target browser.
   *
   * @param string $target
   *   The target browser.
   *
   * @return string
   *   A random user agent entry.
   */
  public function getBrowserRandom($target = self::TARGET_BROWSER_CHROME);

  /**
   * Fetches the latest user agent for the target browser.
   *
   * @param string $target
   *   The target browser.
   *
   * @return string
   *   The latest user agent.
   */
  public function getBrowserLatest($target = self::TARGET_BROWSER_CHROME);

  /**
   * Fetches a random user agent for a random browser.
   *
   * @return string
   *   The random user agent.
   */
  public function getRandom();

}
