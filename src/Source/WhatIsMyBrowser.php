<?php

namespace UserAgent\Source;

use PHPHtmlParser\Dom;
use UserAgent\BaseSource;

class WhatIsMyBrowser extends BaseSource {

  /**
   * A set of parsed DOM lists.
   *
   * @var Dom[]
   */
  protected $targets = [];

  /**
   * Fetches a list of target user agents.
   *
   * @param string $target
   *   The target software.
   *
   * @return \PHPHtmlParser\Dom
   * @throws \PHPHtmlParser\Exceptions\ChildNotFoundException
   * @throws \PHPHtmlParser\Exceptions\CircularException
   * @throws \PHPHtmlParser\Exceptions\CurlException
   * @throws \PHPHtmlParser\Exceptions\StrictException
   */
  protected function fetch($target) {
    if (isset($this->targets[$target]) && $this->targets[$target] instanceof Dom) {
      return $this->targets[$target];
    }

    // Fetch and parse the data.
    $dom = new Dom();
    $dom->load('https://developers.whatismybrowser.com/useragents/explore/software_name/' . $target);

    return $this->targets[$target] = $dom;
  }

  /**
   * {@inheritDoc}
   */
  public function getBrowserAll($target = self::TARGET_BROWSER_CHROME) {
    $agents_list = $this->cache->getCache([$target]);
    if ($agents_list) {
      return $agents_list;
    }

    $dom = $this->fetch($target);
    $agents_list = [];

    $agents = $dom->find('.table-useragents tbody .useragent');
    $versions = $dom->find('.table-useragents tbody td[title]');

    foreach ($agents as $offset => $agent) {
      if ($agent instanceof Dom\HtmlNode) {
        // Attempt to extract a version.
        $agent_version_node = $versions->offsetGet($offset);
        if ($agent_version_node instanceof Dom\HtmlNode) {
          $agent_version = $agent_version_node->text(TRUE);

          $agents_list[$agent_version][] = $agent->text(TRUE);
        }
        else {
          $agents_list[][] = $agent->text(TRUE);
        }
      }
    }

    // Sort the versions.
    ksort($agents_list);

    $this->cache->setCache($agents_list, [$target]);

    return $agents_list;
  }

}
