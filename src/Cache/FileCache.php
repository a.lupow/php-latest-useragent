<?php

namespace UserAgent\Cache;

class FileCache extends RuntimeCache {

  /**
   * Cache source path.
   *
   * @var string
   */
  protected $cache_source;

  /**
   * Cache lifetime in minutes.
   *
   * @var int
   */
  protected $lifetime;

  /**
   * Cache invalidation flag.
   *
   * @var bool
   */
  protected $isInvalidated;

  /**
   * FileCache constructor.
   *
   * @param $cache_dir
   *   A cache directory.
   * @param $lifetime
   *   Cache lifespan in minutes.
   */
  public function __construct($cache_dir, $lifetime = 5000) {
    $this->cache_source = implode('/', [$cache_dir, 'useragents.cache']);
    $this->lifetime = $lifetime;

    // Load the cache right away, if the lifetime is not passed.
    $this->loadCache();
  }

  /**
   * {@inheritDoc}
   */
  public function __destruct() {
    // Update the cache on destruction.
    if ($this->isInvalidated) {
      try {
        file_put_contents($this->cache_source, json_encode($this->storage));
      }
      catch (\Exception $exception) {
        // Ignore exceptions.
      }
    }
  }

  /**
   * Performs cache loading.
   */
  protected function loadCache() {
    if (file_exists($this->cache_source)) {
      $delta = time() - filemtime($this->cache_source);
      if ($delta >= $this->lifetime * 60) {
        return;
      }

      $cache = file_get_contents($this->cache_source);

      // Perform value conversion.
      $this->storage = (array) json_decode($cache);
      foreach ($this->storage as &$software) {
        $software = (array) $software;
      }
    }
  }

  /**
   * {@inheritDoc}
   */
  public function setCache($cache, array $path = NULL) {
    // Mark the cache to be invalidated.
    $this->isInvalidated = TRUE;

    return parent::setCache($cache, $path);
  }

}
