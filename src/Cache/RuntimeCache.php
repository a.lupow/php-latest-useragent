<?php

namespace UserAgent\Cache;

use UserAgent\CacheInterface;

class RuntimeCache implements CacheInterface {

  /**
   * Cache storage.
   *
   * @var array
   */
  protected $storage = [];

  /**
   * {@inheritDoc}
   */
  public function getCache(array $path = NULL, $default_value = NULL) {
    $value = &$this->storage;

    if ($path) {
      foreach ($path as $item) {
        if (isset($value[$item])) {
          $value = &$value[$item];
        }
        else {
          // To make sure the storage is not mutated,
          // return the defaults right away.
          return $default_value;
        }
      }
    }

    return $value;
  }

  /**
   * {@inheritDoc}
   */
  public function setCache($cache, array $path = NULL) {
    $target = &$this->storage;

    if ($path) {
      foreach ($path as $item) {
        if (!isset($target[$item])) {
          $target[$item] = [];
        }

        $target = &$target[$item];
      }
    }

    $target = $cache;

    return $this;
  }

}
