# PhpLatestUseragent

A simple solution for getting either a random or a specific user agent for a specific software.
Currently includes an implementation of _What Is My Browser_ data source.

## Usage

Using the library is as simple as calling one of available methods of either of Source implementations.

```php
$source = new UserAgent\Source\WhatIsMyBrowser();
$user_agent = $source->getRandom();
```

By default a source will use a runtime cache solution, which is barely suitable for any stateless
application, thus you may want to use FileCache backend implementation.

```php
$source = new UserAgent\Source\WhatIsMyBrowser(
    new UserAgent\Cache\FileCache('./cache')
);
```

## Cache

### Runtime cache

By default either of source implementations will use runtime cache, which is stored only in ram.
That cache implementation is as simple as an array storage and requires no explicit initialisation.

### File cache

That caching implementation requires cache directory and optional cache lifetime arguments on itialisation.
